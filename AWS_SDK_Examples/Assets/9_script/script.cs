﻿#region using + Description
//Script to be run in Unity scene. Initialises buttons and methods to be called by them.

using UnityEngine;
using AmazonService;

#endregion

public class script : MonoBehaviour
{
    #region Variables
    
    public string RecordName = "RecordName";
    public string Value = "Value";
    public string Bucketname;
    public string Foldername;
    public string DataPath;
    public string Filename = "Music.mp3";
    public string status;
     public string NumberOfSync = "";
     public string SNSStatus = "";
    public string DatasetName = "PlayerData";
    public string lessontobuy = "0";
    public string language = "ENG";
    #endregion

    void Awake()
    {
        //<summary>
        //Called when script is instance is loaded. Initialises variables before app starts.
        //</summary>

    }
    void Start()
    {
        DataPath = Application.persistentDataPath; //Sets appoprate datapath (where downloads stored) depending on platform
        AmazonServices.Start();
    }

    void OnGUI()
    {
        #region input fields
        //Input boxes
        GUI.TextArea(new Rect(50, 230, 120, 20), "Bucketname");
        Bucketname = GUI.TextField(new Rect(10, 250, 200, 20), Bucketname, 125);

        GUI.TextArea(new Rect(50, 280, 120, 20), "Foldername");
        Foldername = GUI.TextField(new Rect(10, 300, 200, 20), Foldername, 250);

        GUI.TextArea(new Rect(50, 320, 120, 20), "Filename");
        Filename = GUI.TextField(new Rect(10, 340, 200, 20), Filename, 25);

        GUI.TextArea(new Rect(320, 360, 120, 20), "DataInput");
        DataPath = GUI.TextField(new Rect(10, 380, 900, 150), DataPath, 1000);



        //Mitte
        GUI.TextArea(new Rect(320, 230, 120, 20), "SNSStatus");
        SNSStatus = GUI.TextField(new Rect(280, 250, 200, 20), SNSStatus, 125);

        GUI.TextArea(new Rect(320, 280, 120, 20), "Status");
        status = GUI.TextField(new Rect(280, 300, 200, 20), status, 125);

        GUI.TextArea(new Rect(320, 320, 120, 20), "NumberOfSync");
        NumberOfSync = GUI.TextField(new Rect(280, 340, 200, 20), NumberOfSync, 125);



        //Rechts
        GUI.TextArea(new Rect(600, 230, 120, 20), "Dataset Name");
        DatasetName = GUI.TextField(new Rect(560, 250, 200, 20), DatasetName, 125);

        GUI.TextArea(new Rect(600, 280, 120, 20), "RecordName Sync");
        RecordName = GUI.TextField(new Rect(560, 300, 200, 20), RecordName, 125);

        GUI.TextArea(new Rect(600, 320, 120, 20), "RecordValue Sync");
        Value = GUI.TextField(new Rect(560, 340, 200, 20), Value, 125);

        GUI.TextArea(new Rect(840, 230, 120, 20), "Lesson To Buy");
        lessontobuy = GUI.TextField(new Rect(840, 250, 200, 20), lessontobuy, 125);

        GUI.TextArea(new Rect(840, 280, 120, 20), "Language");
        language = GUI.TextField(new Rect(840, 300, 200, 20), language, 125);
        #endregion

        #region Buttons
        //Buttons
        if (GUI.Button(new Rect(10, 10, 120, 80), "Upload"))
        {

            AmazonServices.InitiateDownloadObject(DataPath , Foldername, Filename, "Upload");
        }
        if (GUI.Button(new Rect(10, 110, 120, 80), "Download"))
        {

            AmazonServices.InitiateDownloadObject(DataPath , Foldername, Filename, "Download");
        }
        if (GUI.Button(new Rect(150, 10, 120, 80), "Put Data"))
        {
            //statusMessage = "Saving to CognitoSync Cloud";
            AmazonServices.PutDataset(RecordName, Value, DatasetName);
            //AmazonServices.Sync("PlayerData");
        }
        if (GUI.Button(new Rect(150, 110, 120, 80), "Get Value"))
        {
            //statusMessage = "Saving to CognitoSync Cloud";
            //AmazonServices.PushDataset(RecordName, Value, "PlayerData");
            Value = AmazonServices.GetValue(RecordName, DatasetName);

        }
        if (GUI.Button(new Rect(290, 10, 120, 80), "Sync"))
        {
            AmazonServices.Sync(DatasetName);
            //NumberOfSync = AmazonServices.GetValue("NumberOfSync");

        }
        if (GUI.Button(new Rect(290, 110, 120, 80), "Connect to Facebook"))
        {
            AmazonServices.FacebookConnect();

        }
        if (GUI.Button(new Rect(430, 10, 120, 80), "Delete RecordName"))
        {
            //statusMessage = "Saving to CognitoSync Cloud";
            //AmazonServices.PushDataset(RecordName, Value, DatasetName);
            AmazonServices.DeleteRecord(RecordName);

        }
        if (GUI.Button(new Rect(430, 110, 120, 80), "Delete Dataset"))
        {
            AmazonServices.DeleteDataset(DatasetName);

        }

        if (GUI.Button(new Rect(570, 10, 120, 80), "Reg SNS"))
        {
            AmazonServices.RegisterDeviceSNS();

        }

        if (GUI.Button(new Rect(570, 110, 120, 80), "UnReg SNS"))
        {
            AmazonServices.UnregisterDeviceSNS();
        }

        if (GUI.Button(new Rect(710, 10, 120, 80), "AnalyticRec"))
        {
            AmazonServices.AnalyticRec();
        }

        if (GUI.Button(new Rect(710, 110, 120, 80), "AnalyticMonetization"))
        {
            AmazonServices.AnalyticMonetization();

        }

        if (GUI.Button(new Rect(850, 10, 120, 80), "Buy"))
        {
            Store.CheckLessonBought(lessontobuy, DatasetName);
        }

        if (GUI.Button(new Rect(850, 110, 120, 80), "Sync/Download"))
        {
            Store.DownloadLessons(DataPath, Foldername, DatasetName, language);

        }


        #endregion

    }


}

