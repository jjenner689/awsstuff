﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
namespace AmazonService
{
    public static class Store

    {
        public static Lesson CreateLessonObj(string Cost, string Bought)
        {
            //<summary>
            //Iniatiates and returns a lesson object 
            //</summary>
            Lesson LessonObj = new Lesson();
            LessonObj.Cost = Int32.Parse(Cost);
            LessonObj.Bought = Bought;
            return LessonObj;
        }

        public static Dictionary<string, Lesson> StringToDic(string record)
        {
            //<summary>
            //Takes string array and returns a dictionary of lesson objects
            //</summary>
            Dictionary<string, Lesson> LessonDic = new Dictionary<string, Lesson>();
            string[] array = record.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in array)
            {
                string[] newarray = item.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                LessonDic.Add(newarray[0], CreateLessonObj(newarray[1], newarray[2]));
            }
            return LessonDic;
        }

        public static void printdic(Dictionary<string, Lesson> dic)
        {
            //<summary>
            //Prints all attributes for each dictionary value
            //</summary>
            foreach (KeyValuePair<string, Lesson> item in dic)
            {
                Debug.Log(item.Key + " " + item.Value.Cost + " " + item.Value.Bought);
            }
        }

        public static string CreateStringArray(Dictionary<string, Lesson> dic)
        {
            //<summary>
            //Returns string array from dictionary of lesson objects
            //</summary>
            var array = "";
            foreach (KeyValuePair<string, Lesson> item in dic)
            {
                var lesson = "";
                lesson += item.Key + "," + item.Value.Cost + "," + item.Value.Bought + "|";
                array += lesson;
            }
            return array;
        }

        public static void DownloadLessons(string DataPath, string Foldername, string DatasetName, string language)
        {
            //<summary>
            //Checks for bought lessons and downloads any which are yet to be downloaded
            //</summary>
            var string_array = AmazonServices.GetValue("LessonData", DatasetName);
            var dic = StringToDic(string_array);
            printdic(dic);
            foreach (KeyValuePair<string, Lesson> item in dic)
            {
                var Filename = "Lesson" + item.Key + ".mp3"; //need to set as downloaded
                var path = (DataPath + "/" + Foldername + "/" + Filename);
                if ((item.Value.Bought == "Y") && (!File.Exists(path)))
                {
                    Debug.Log(path);
                    AmazonServices.InitiateDownloadObject(DataPath, Foldername, Filename, "Download", language);
                    string_array = CreateStringArray(dic);
                    AmazonServices.PlayerData.Put("LessonData", string_array);
                }
                else if (item.Value.Bought == "N")
                {
                    Debug.Log("Not Bought the lesson!");
                }

                else
                {
                    Debug.Log("Already downloaded!");
                }
            }

        }

        public static void CheckLessonBought(string lesson, string DatasetName)
        {
            //<summary>
            //Checks if lesson in dataset is bought and logs appropriate response
            //</summary>
            Debug.Log("Buy");
            Debug.Log(lesson);
            var string_array = AmazonServices.GetValue("LessonData", DatasetName);
            var dic = StringToDic(string_array);
            if (!(dic.ContainsKey(lesson)))
            {
                Debug.Log("Not a valid lesson choice");
            }
            else if (dic[lesson].Cost >= Int32.Parse(AmazonServices.GetValue("Credits", DatasetName)))
            {
                Debug.Log("Not enough credits");
            }

            else if (dic[lesson].Bought == "Y")
            {
                Debug.Log("Lesson already bought");
            }

            else
            {
                Debug.Log("Extra stuff here");
                BuyLesson(lesson, DatasetName);
            }
        }

        public static void BuyLesson(string lesson, string DatasetName)
        {
            //<summary>
            //Sets a lesson's bought attribute to Y and deducts appropriate credits
            //</summary>
            var string_array = AmazonServices.GetValue("LessonData", DatasetName);
            var dic = StringToDic(string_array);
            dic[lesson].SetBought();
            var credits = (Int32.Parse(AmazonServices.GetValue("Credits", DatasetName)) - dic[lesson].Cost).ToString();
            string_array = CreateStringArray(dic);
            AmazonServices.PlayerData.Put("LessonData", string_array);
            AmazonServices.PlayerData.Put("Credits", credits);
            Debug.Log(credits);
            Debug.Log(string_array);
        }

    }

    public class Lesson
    {
        //<summary>
        //Lesson object with Cost and Bought attributes
        //</summary>
        public int Cost { get; set; }
        public string Bought { get; set; }

        public void SetBought()
        {
            Bought = "Y";
        }
    }
}