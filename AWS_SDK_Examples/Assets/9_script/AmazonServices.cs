﻿#region Description
//Consists of 4 classes:

//The DownloadObject containing all the individual information required for 
//each download.

//The Listener for the event handler. Listens for when an individual down/upload has finished

//The FeedbackEventArgs contains variables to be passed through the event handler

//The DownloadManager handles downloads and uploads by placing instances of DownloadObject in 
//a queue so that only one object is down/uploaded at a time
#endregion
#region Using Stuff
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.S3.Util;

using Amazon.CognitoSync.SyncManager;
using Amazon.CognitoSync;
using Amazon.CognitoIdentity.Model;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.MobileAnalytics.MobileAnalyticsManager;
using Amazon.Util.Internal;

using Facebook;
using Facebook.Unity;
#endregion
using System.Globalization;
namespace AmazonService
{
    public static class AmazonServices
    {
        //<summary>
        //Class contains methods which interact with aws services
        //</summary>

        #region Variables to change depending on your aws and facebook accounts
        //SNS Variables
        public static string AndroidPlatformApplicationArn = "arn:aws:sns:us-east-1:645062636366:app/GCM/Test";
        public static string iOSPlatformApplicationArn = "";
        public static string GoogleConsoleProjectId = "820981410028";
        public static string appId = "7328a4e7f30240119c48fd3628c659b5";
        
        //Cognito Variables
        public static string IdentityPoolId = "us-east-1:208e1c9b-8f6a-4881-8d9c-018d370db552";
        public static RegionEndpoint Region = RegionEndpoint.USEast1;
        public static string RegionString = "USEast1";
        public static string StandardDatasetString = "PlayerData";

        //S3 Variables
        public static string S3UploadBucketName = "bommic"; 
        public static string S3DownloadBucketName = "bommic";

        public static int DaysUntilDeviceExpire = 30;

        #endregion

        #region Fixed Variables (Don't need to change these)
        private static IAmazonSimpleNotificationService _snsClient;
        static private MobileAnalyticsManager analyticsManager;
        private static Queue<DownloadObject> DownloadQueue;
        public static CognitoAWSCredentials _credentials;
        public static Dataset PlayerData;
        private static string _endpointArn;
        private static CognitoSyncManager syncManager;
        public static bool SyncActive;
        public static bool S3Active;
        public static bool DebugFlag;
        public static bool Online;
        public static bool DebugFileLoaded;
        public static S3_UP_Down S3Debug;
        private static IAmazonS3 _s3Client;
        public static string ID = SystemInfo.deviceUniqueIdentifier;
        public static int DeviceNumber;
        public static Dataset StandardDataset;
        #endregion

        #region Credentials
        public static CognitoAWSCredentials Credentials //Checks for credentials. If non existent, requests new ones.
        {
            //<summary>
            //Checks for credentials. If non existent, requests new ones.
            //</summary>
            get
            {
                if (_credentials == null)
                    _credentials = new CognitoAWSCredentials(IdentityPoolId, Region);
                return _credentials;
            }
        }

        public static IAmazonS3 Client
        {
            //<summary>
            //Initiates an S3 client
            //</summary>
            get
            {
                if (_s3Client == null)
                    _s3Client = new AmazonS3Client(AmazonServices.Credentials, AmazonServices.Region);
                return _s3Client;
            }
        }

        private static IAmazonSimpleNotificationService SnsClient
        {
            //<summary>
            //Iniates an SNS client
            //</summary>
            get
            {
                if (_snsClient == null)
                    _snsClient = new AmazonSimpleNotificationServiceClient(Credentials, Region);
                return _snsClient;
            }
        }
        #endregion

        #region Constructor
        static AmazonServices()
        {
            //<summary>
            //Constructor
            //</summary>

            OnOfflineSwitch();
            
            //Initiates an instance of sync manager (Used when syncing with cognito)
            syncManager = new CognitoSyncManager(Credentials, new AmazonCognitoSyncConfig
            {
                RegionEndpoint = Region
            });

            //Data (which is unique to individual users) is stored locally in a dataset
            PlayerData = syncManager.OpenOrCreateDataset(StandardDatasetString);

            //Checks to see if dataset records have been created yet and syncs if not
            if ((GetValue("NumberOfSync", StandardDatasetString) == null) ||
                (GetValue("NumberOfSync", StandardDatasetString) == "") ||
                (GetValue("Credits", StandardDatasetString) == null) ||
                (GetValue("Credits", StandardDatasetString) == "") ||
                (GetValue("LessonData", StandardDatasetString) == null) ||
                (GetValue("LessonData", StandardDatasetString) == "") ||
                (GetValue("DeviceID0", StandardDatasetString) == null) ||
                (GetValue("DeviceID0", StandardDatasetString) == ""))
            {
                Sync(StandardDatasetString);
            }

            //Sets the number of individual devices attactched to one account
            DeviceNumber=0;
            while ((GetValue("DeviceID" + DeviceNumber, StandardDatasetString) != ID) && (GetValue("DeviceID" + DeviceNumber, StandardDatasetString) != ""))
                DeviceNumber++;

            //Saves the facebook secret key (string)
            if ((PlayerData.Get("FB-Loginkey") != "")&&(PlayerData.Get("FB-Loginkey") != null))
                Credentials.AddLogin("graph.facebook.com", PlayerData.Get("FB-Loginkey"));

        }
        public static void Start() { }
        #endregion

        #region On/Offlinechecker and Debugchecker
        public static bool OnOfflineSwitch()
        {
            //<summary>
            //Sets debug flags and returns the status of the internet connection 
            //</summary>
            if (CheckInternetConnection() == true)
            {
                Debug.Log("Internet Connection is OK");
                if (DownloadQueue != null)
                {
                    if (DownloadQueue.Count >= 1 && Online == false)
                    {
                        HandleQueueObject(); //Restarts any previously queued downloads
                    }
                }
                Online = true;
                S3Debug = new S3_UP_Down();
                S3Debug.DownloadDebug();
                return true;
            }
            else
            {
                SyncActive = false;
                S3Active = false;
                Online = false;
                DebugFlag = false;
                DebugFileLoaded = false;
                Debug.Log("No Internet Connection");
                return false;

            }

        }
        public static string GetHtmlFromUri(string resource)
        {
            //<summary>
            //Opens a WebPage and returns the source code as a string if connected
            //Returns empty string if can't connect
            //</summary>
            string html = string.Empty;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                    bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                    if (isSuccess)
                    {
                        using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                        {
                            char[] cs = new char[80];
                            reader.Read(cs, 0, cs.Length);
                            foreach (char ch in cs)
                            {
                                html += ch;
                            }
                        }
                    }
                }
            }
            catch
            {
                return "";
            }
            return html;
        }
        public static bool CheckInternetConnection()
        {
            //<summary>
            //Checks the Internet Connection
            //Returns true if connected and false if not
            //</summary>

            string HtmlText = GetHtmlFromUri("http://google.com");
            if (HtmlText == "")
            {
                return false;
            }
            else if (!HtmlText.Contains("schema.org/WebPage"))
            {
                return true;//If redirected
            }
            else
            {
                return true;//If connects
            }


        }
        #endregion

        #region DownloadManager
    
        public static void HandleQueueObject()
        {
            //<summary>
            //Pulls DownloadObject from queue, prepares it and then starts down/upload
            //</summary>
            if (DownloadQueue.Count != 0)
            {
                DownloadObject Obj = new DownloadObject();
                Obj = DownloadQueue.Dequeue(); //Retrieve next download object from queue
                Listener listener = new Listener();
                S3_UP_Down S3UpDownInstance = new S3_UP_Down(); //Start instance of S3_Up_Down (contains up/download methods)
                listener.Subscribe(S3UpDownInstance);

                if (Obj.type == "Download")
                {
                    _credentials = Credentials;
                    S3UpDownInstance.DownloadFile(Obj.DeviceFolder, Obj.DownFilename, Obj.TargetFolder); //Start download
                }
                else if (Obj.type == "Upload")
                {
                    _credentials = Credentials;
                    S3UpDownInstance.CheckFileExists(Obj.DownFilename, Obj.DeviceFolder, Obj.IsString); //Start upload


                }
            }
            else
            {
                GC.Collect(); // Garbage Collector cleans up (old instances of S3_Up_Down)
            }
        }
        public static int InitiateDownloadObject(string Datapath, string Localpath, string filename, string type)
        {
            return InitiateDownloadObject(Datapath, Localpath, filename, type, "");
        }
        public static int InitiateDownloadObject(string Datapath,string Localpath, string filename, string type, string target_folder)
        {
            Datapath = FixDataPath(Datapath);
            Localpath = FixFoldername(Localpath);
            filename = FixFileName(filename);
            Localpath = Datapath + Localpath;

            //<summary>
            //Creates DownloadObject, sets it's variables and sends to queue


            //</summary>
            if (OnOfflineSwitch())
            {
                if (DebugFileLoaded == true)
                {
                    if (S3Active == true)
                    {
                        DownloadObject x = new DownloadObject();
                        x.DeviceFolder = Localpath;
                        x.DownFilename = filename;
                        x.TargetFolder = target_folder;
                        x.type = type;
                        AddToQueue(x);
                        return 0;
                    }
                    else
                    {
                        Debug.Log("S3 is Disabled");
                        return 2;
                    }
                }
            }
            return 1;
        }
        
        static void AddToQueue(DownloadObject Obj)
        {
            //<summary>
            //Adds DownloadObject to queue. If queue doesn't exists, creates new instance
            //</summary>       
            if (DownloadQueue == null)
            {
                DownloadQueue = new Queue<DownloadObject>();
                DownloadQueue.Enqueue(Obj);
                HandleQueueObject();
            }
            else
            {
                DownloadQueue.Enqueue(Obj);
                if (DownloadQueue.Count == 1)
                {
                    HandleQueueObject();
                }
            }
        }
        public static string FixDataPath(string datapath)
        {
            //<summary>
            //Corects datapath if not written in correct format
            //</summary>
            if (datapath.Length > 0)
            {
                if (datapath.Substring(datapath.Length - 1) != "/")
                    datapath += "/";
            }
            return datapath;
        }

        public static string FixFoldername(string foldername)
        {
            //<summary>
            //Corects foldername if not written in correct format
            //</summary>
            if (foldername.Length > 0)
            {

                if (foldername.Substring(0, 1) == "/")
                    foldername = foldername.Substring(1);
                if (foldername.Substring(foldername.Length - 1) != "/")
                    foldername += "/";

            }
            return foldername;
        }

        public static string FixFileName(string filename)
        {
            //<summary>
            //Corects filename if not written in correct format
            //</summary>
            if (filename.Length > 0)
            {

                if (filename.Substring(0, 1) == "/")
                    filename = filename.Substring(1);
            }
            return filename;
        }
        #endregion

        #region Cognito Sync
        //Start of Cognito Sync

        public static Dataset GetDatasetName(string stringDataset)
        {
            //<summary>
            //Returns name of the dataset
            //</summary>   

            if (stringDataset == StandardDatasetString)
                return PlayerData;
            return null;
        }


        public static string GetValue(string RecordName, string DatasetName)
        {
            //<summary>
            //Returns the corresponding value of the given record name
            //</summary>   

            Dataset DatasetValue = GetDatasetName(DatasetName);

            string Value = DatasetValue.Get(RecordName);

            if (Value == null)
                return "";
            return Value;
        }
        public static string GetValue(string RecordName)
        {
            //<summary>
            //Returns the corresponding value of the given record name
            //</summary>   
            return GetValue(RecordName, StandardDatasetString);
        }

        public static void DeleteRecord(string RecordName, string DatasetName)
        {
            //<summary>
            //Deletes the given record from the dataset
            //</summary>   
            Dataset DatasetValue = GetDatasetName(DatasetName);
            DatasetValue.Remove(RecordName);
        }
        public static void DeleteRecord(string RecordName)
        {
            //<summary>
            //Deletes the given record from the dataset
            //</summary> 
            DeleteRecord(RecordName, StandardDatasetString);
        }

        public static void DeleteDataset(string DatasetName)
        {
            //<summary>
            //Deletes the given dataset
            //</summary> 
            Dataset DatasetValue = GetDatasetName(DatasetName);
            DatasetValue.Delete();
        }
        public static void DeleteDataset()
        {
            //<summary>
            //Deletes the given dataset
            //</summary> 
            DeleteDataset(StandardDatasetString);

        }

        public static int Sync()
        {
            //<summary>
            //Sync the Local Dataset with Cognito
            //</summary> 

            return Sync(StandardDatasetString);
        }
        public static int Sync(string DatasetName)
        {
            //<summary>
            //Sync the Local Dataset with Cognito
            //</summary> 
            if (OnOfflineSwitch())
            {
                if (DebugFileLoaded == true)
                {
                    if (SyncActive == true)
                    {
                        Dataset DatasetValue = GetDatasetName(DatasetName);

                        _credentials = Credentials;
                        DatasetValue.Synchronize();

                        //If device yet to sync, create approprate player data
                        if (GetValue("NumberOfSync", DatasetName) == "")
                            ConstructPlayerData(DatasetName, DatasetValue);

                        DatasetValue.Put("NumberOfSync", Convert.ToString(Convert.ToInt32(GetValue("NumberOfSync", DatasetName)) + 1));

                        CleanOldDevices(DatasetValue, DatasetName);

                        //Add sync date
                        DatasetValue.Put("DeviceDateofLastUse"+DeviceNumber, System.DateTime.Now.ToShortDateString());

                        if (DeviceNumber > 0)
                            DatasetValue.Put("MultipleDevice", "1");

                        //If it's a new Device, set the ID and synchonize again
                        if (GetValue("DeviceID" + DeviceNumber, DatasetName) == "")
                        {
                            DatasetValue.Put("NumberOfDevices", Convert.ToString(Convert.ToInt32(0 + 1)));
                            DatasetValue.Put("DeviceID" + DeviceNumber, ID);
                            DatasetValue.Put("NumberOfSync", Convert.ToString(Convert.ToInt32(GetValue("NumberOfSync", DatasetName)) + 1));
                            DatasetValue.Synchronize();


                        }
                        return 0;



                    }
                    else
                    {
                        Debug.Log("Sync is Disabled");
                    }

                }
            }
            return 1;
        }

        public static void ConstructPlayerData(string DatasetName, Dataset DatasetValue)
        {
            //<summary>
            //Create starting player data
            //</summary>
            DatasetValue.Put("NumberOfSync", "0");
            DatasetValue.Put("NumberOfSync", Convert.ToString(Convert.ToInt32(GetValue("NumberOfSync", DatasetName)) + 1));
            DatasetValue.Put("Credits", "20");
            S3_UP_Down S3UpDownInstance = new S3_UP_Down();
            var LessonData = S3UpDownInstance.DownloadStarter("ENG"); //make eng an argument
            PlayerData.Put("LessonData", LessonData);
        }
        
        public static void CleanOldDevices(Dataset DatasetValue, string DatasetName)
        {
            //<summary>
            //Removes data from any devices inactive for given days
            //</summary> 

            if (GetValue("NumberOfDevices", DatasetName) == "")
                DatasetValue.Put("NumberOfDevices", "1");

            for (int x = 0; x < Convert.ToInt32(GetValue("NumberOfDevices", DatasetName)); x++)
                if (GetValue("DeviceDateofLastUse" + x, DatasetName) != "")
                {
                    DateTime date = Convert.ToDateTime(GetValue("DeviceDateofLastUse" + 0, DatasetName));
                    if ((System.DateTime.Now - date).Days > DaysUntilDeviceExpire)
                    {
                        DeleteDevice(x, DatasetName, DatasetValue);
                        x = Convert.ToInt32(GetValue("NumberOfDevices", DatasetName));
                        CleanOldDevices(DatasetValue, DatasetName);
                    }
                }
        }

        public static void DeleteDevice(int DeviceNumber, string DatasetName, Dataset DatasetValue)
        {
            //<summary>
            //Removes data for device
            //</summary> 
            int NumerOfDevices = Convert.ToInt32(GetValue("NumberOfDevices", DatasetName)) - 1;
            for (int x = DeviceNumber; x < NumerOfDevices; x++)
            {
                DatasetValue.Put("DeviceID" + x, GetValue("DeviceID" + x + 1, DatasetName));
                DatasetValue.Put("DeviceDateofLastUse" + x, GetValue("DeviceDateofLastUse" + x + 1, DatasetName));
            }
            DatasetValue.Put("NumberOfDevices", Convert.ToString(NumerOfDevices));
            DeleteRecord("DeviceID" + NumerOfDevices, DatasetName);
            DeleteRecord("DeviceDateofLastUse" + NumerOfDevices, DatasetName);
            if (NumerOfDevices <= 1)
                DatasetValue.Put("MultipleDevice", "0");
        }

        public static void PutDataset(string RecordName, string Value, string DatasetName)
        {
            //<summary>
            //Sets appropriate record for given data set
            //</summary> 
            Dataset DatasetValue = GetDatasetName(DatasetName);
            DatasetValue.Put(RecordName, Value);

        }

        public static void PutDataset(string RecordName, string Value)
        {
            //<summary>
            //Sets appropriate record for given data set
            //</summary> 
            PutDataset(RecordName, Value, StandardDatasetString);
        }
        //END OF Cognito Sync
        #endregion

        #region Facebook
        //FACEBOOK Connect
        public static void FacebookConnect()
        {


            if (!FB.IsInitialized)
            {
                FB.Init(delegate ()
                {
                    Debug.Log("Start to ReConnect to Facebook");

                    // shows to connect the current identityid or create a new identityid with facebook authentication
                    FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, FacebookLoginCallback);
                });
            }
            else
            {
                Debug.Log("Start to Connect to Facebook");
                FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, FacebookLoginCallback);
            }
        }
        private static void FacebookLoginCallback(IResult result)
        {
            Debug.Log("FB.Login completed");

            if (result.Error != null || !FB.IsLoggedIn)
            {
                Debug.LogError(result.Error);

            }
            else
            {
                Debug.Log("Write Connect to Credentials");
                Credentials.AddLogin("graph.facebook.com", AccessToken.CurrentAccessToken.TokenString);
                PlayerData.Put("FB-Loginkey", AccessToken.CurrentAccessToken.TokenString);
            }
        }
        //END FACEBOOK Connect
        #endregion

        #region SNS
        ///SNS Stuff
        public static void RegisterDeviceSNS()
        {
            //<summary>
            //Register Device for Amazon Simple Notification Service
            //</summary> 
#if UNITY_ANDROID
            if (string.IsNullOrEmpty(GoogleConsoleProjectId))
            {
                Debug.Log("sender id is null");
                return;
            }
            GCM.Register((regId) =>
            {

                if (string.IsNullOrEmpty(regId))
                {
                    Debug.Log("Failed to get the registration id");
    
                }

                //ResultText.text = string.Format(@"Your registration Id is = {0}", regId);

                SnsClient.CreatePlatformEndpointAsync(
                    new CreatePlatformEndpointRequest
                    {
                        Token = regId,
                        PlatformApplicationArn = AndroidPlatformApplicationArn,
                        CustomUserData = ID
                    },
                    (resultObject) =>
                    {
                        if (resultObject.Exception == null)
                        {
                            CreatePlatformEndpointResponse response = resultObject.Response;
                            _endpointArn = response.EndpointArn;
                             Debug.Log("Platform endpoint arn is = "+response.EndpointArn);
                        }
                    }
                );
            }, GoogleConsoleProjectId);
#elif UNITY_IOS
#if UNITY_5
                UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);
#else
                NotificationServices.RegisterForRemoteNotificationTypes(RemoteNotificationType.Alert|RemoteNotificationType.Badge|RemoteNotificationType.Sound);
#endif
                CancelInvoke("CheckForDeviceToken");
                InvokeRepeating("CheckForDeviceToken",1f,1f);
#endif
        }
        private static void CheckForDeviceToken()
        {
#if UNITY_IOS
#if UNITY_5
                var token = UnityEngine.iOS.NotificationServices.deviceToken;
                var error = UnityEngine.iOS.NotificationServices.registrationError;
#else
                var token = NotificationServices.deviceToken;
                var error = NotificationServices.registrationError;
#endif
                if(count>=10 || !string.IsNullOrEmpty(error))
                {
                    CancelInvoke("CheckForDeviceToken");
                    Debug.Log(@"Cancel polling");
                    return;
                }
            
                if(token!=null)
                {
                    deviceToken = System.BitConverter.ToString(token).Replace("-","");
                    Debug.Log("device token  = " + deviceToken);
                    ResultText.text = string.Format(@"Your device token is = {0}", deviceToken);
                    SnsClient.CreatePlatformEndpointAsync(
                        new CreatePlatformEndpointRequest
                        {
                            Token = deviceToken,
                            PlatformApplicationArn = iOSPlatformApplicationArn
                            CustomUserData = ID
                        },
                        (resultObject) =>
                        {
                            if(resultObject.Exception==null)
                            {
                                CreatePlatformEndpointResponse response = resultObject.Response;
                                _endpointArn = response.EndpointArn;
                                ResultText.text += string.Format("\n Subscribed to Platform endpoint arn = {0}", response.EndpointArn);
                            }
                        }
                    );
                
                    CancelInvoke("CheckForDeviceToken");
                }
                count++;
#endif
        }
        public static string UnregisterDeviceSNS()
        {
            if (!string.IsNullOrEmpty(_endpointArn))
            {
                SnsClient.DeleteEndpointAsync(
                    new DeleteEndpointRequest
                    {
                        EndpointArn = _endpointArn
                    },
                    (resultObject) =>
                    {
                        if (resultObject.Exception == null)
                        {
                            Debug.Log("Deleted the endpoint. You will not get any new notifications");
                            //return "You will not get any new notifications";
                        }
                    }
                );
            }
            return "SNS cant be unreg";
        }
        // SNS END
        #endregion

        #region Mobile Analytics
        // Mobile  Analytics Functions
        public static void AnalyticRec()
        {
            CustomEvent customEvent = new CustomEvent("level_complete");

            customEvent.AddAttribute("LevelName", "Level1");
            customEvent.AddAttribute("CharacterClass", "Warrior");
            customEvent.AddAttribute("Successful", "True");
            customEvent.AddMetric("Score", 12345);
            customEvent.AddMetric("TimeInLevel", 64);

            analyticsManager.RecordEvent(customEvent);


        }
        public static void AnalyticMonetization()
        {
            MonetizationEvent monetizationEvent = new MonetizationEvent();
            monetizationEvent.Quantity = 3.0;
            monetizationEvent.ItemPrice = 1.99;
            monetizationEvent.ProductId = "ProductId123";
            monetizationEvent.ItemPriceFormatted = "$1.99";
            monetizationEvent.Store = "Apple";
            monetizationEvent.TransactionId = "TransactionId123";
            monetizationEvent.Currency = "USD";

            analyticsManager.RecordEvent(monetizationEvent);
        }
        static void OnApplicationFocus(bool focus)
        {
            if (analyticsManager != null)
            {
                if (focus)
                {
                    analyticsManager.ResumeSession();
                }
                else
                {
                    analyticsManager.PauseSession();
                }
            }
        }
        // END Mobile  Analytics Functions
        #endregion
    }
    #region extra Classes
    public class GCM
    {
        //<summary>
        //The Class for SNS Register und Unregister
        //</summary>
        private const string CLASS_NAME = "com.amazonaws.unity.AWSUnityGCMWrapper";

        public static void Register(Action<string> OnRegisterCallback, params string[] senderId)
        {
#if UNITY_ANDROID
            using (AndroidJavaClass cls = new AndroidJavaClass(CLASS_NAME))
            {
                string senderIds = string.Join(",", senderId);
                string regId = cls.CallStatic<string>("register", senderIds);
                Debug.Log("regId = " + regId);
                if (OnRegisterCallback != null)
                    OnRegisterCallback(regId);
            }
#endif
        }

        public static void Unregister()
        {
#if UNITY_ANDROID
            if (Application.platform == RuntimePlatform.Android)
            {
                using (AndroidJavaClass cls = new AndroidJavaClass(CLASS_NAME))
                {
                    cls.CallStatic("unregister");
                }
            }
#endif
        }



    }
    public class DownloadObject
    {
        //<summary>
        //Contains data required for a download/upload. To be placed in a queue.
        //</summary>
        public string DeviceFolder;
        public string DownFilename;
        public string type;
        public string TargetFolder;
        public bool IsString;

        public DownloadObject(string Adress, string Folder)
        {
            DeviceFolder = Adress;
            DownFilename = Folder;
        }
        public DownloadObject()
        { }
    }
    public class FeedbackEventArgs : EventArgs
    {
        //<summary>
        //To be used as an argument in the event handler to pass through required data
        //</summary>
        public S3_UP_Down S3_instance;
        public string data = ",";
        public string type;
    }
    public class Listener
    {
        //<summary>
        //Listener for event handler. Executes method HeardIt when event triggered.
        //</summary>

        public void Subscribe(S3_UP_Down m)
        {
            m.Handle += new S3_UP_Down.Handler(HeardIt);
        }

        private void HeardIt(S3_UP_Down m, FeedbackEventArgs e)
        {
            if (e.type == "download")
            {
                Debug.Log("Download finished");
            }
            else
            {
                Debug.Log("Upload finished");
            }
            e.S3_instance = null; //Allocate to null so old instance is ready for garbage collection

            AmazonServices.HandleQueueObject(); //Next down/upload
        }
    }
    #endregion
}
