﻿#region Description and Variables
//Contatins S3_Up_Down class which contains methods to Upload and Download to
//and from S3 

using System;
using System.Globalization;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.S3.Util;
using AmazonService;
using Facebook;
#endregion
namespace AmazonService
{

    public class S3_UP_Down
    {
        #region Variables
        //S3 bucket information
        public string S3UploadBucketName = AmazonServices.S3UploadBucketName;
        public string S3DownloadBucketName = AmazonServices.S3DownloadBucketName;
        public event Handler Handle;
        public FeedbackEventArgs e;
        public delegate void Handler(S3_UP_Down m, FeedbackEventArgs e);

        ~S3_UP_Down() { } //Destructor


        #endregion

        public void DownloadFile(string Localpath, string targetname, string BucketFolder)
        {
            //overload function
            DownloadFile(Localpath, targetname, BucketFolder, S3DownloadBucketName);
        }

        public void DownloadFile(string Localpath, string targetname, string BucketFolder, string BucketName)
        {
            //<summary>
            //Downloads object (targetname) from S3 and writes to Localpath directory
            //</summary>
            e = new FeedbackEventArgs();
            e.type = "download";
            var target_path = BucketName;
            if (BucketFolder != "")
            {
                target_path += "/" + BucketFolder;
            }

            AmazonServices.Client.GetObjectAsync(target_path, targetname, (responseObj) =>
            {
                if (Localpath.Length > 0)
                {
                    if (Localpath.Substring(Localpath.Length - 1) != "/")
                        Localpath += "/";
                    if (!Directory.Exists(Localpath))
                    {
                        Directory.CreateDirectory(Localpath);
                    }
                    byte[] data; //create byte array
                    var response = responseObj.Response;
                    if (response.ResponseStream != null)
                    {
                        using (BinaryReader reader = new BinaryReader(response.ResponseStream))
                        {
                            const int bufferSize = 4096;
                            using (var ms = new MemoryStream())
                            {
                                byte[] buffer = new byte[bufferSize];
                                int count;
                                while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                                    ms.Write(buffer, 0, count);
                                data = ms.ToArray();
                                System.IO.File.WriteAllBytes(Localpath + targetname, data);
                                Handle(this, e); //Send download finished event to event handler
                            }
                        }
                    }
                }
            });
        }
        public void DownloadDebug()
        {
            string result = string.Empty;
            string[] debug;
            using (var webClient = new System.Net.WebClient())
            {
                result = webClient.DownloadString("http://s3.amazonaws.com/"+S3DownloadBucketName+"/Debug");
            }
            debug = result.Split(new string[] { "\r\n","\r","\n" }, StringSplitOptions.RemoveEmptyEntries);
            Debug.Log("Download DebugFile");
            AmazonServices.SyncActive = (debug[1] == "1");
            AmazonServices.S3Active = (debug[3] == "1");
            AmazonServices.DebugFlag = (debug[5] == "1");
            AmazonServices.DebugFileLoaded = true;
        }

        public string DownloadStarter(string language)
        {
            string result = string.Empty;
            string[] starter;
            var string_array = ""; //To be saved in DataSet
            using (var webClient = new System.Net.WebClient())
            {
                result = webClient.DownloadString("http://s3.amazonaws.com/" + S3DownloadBucketName + "/" + language + "/Starter"); //Must make ENG a variable
            }
            starter = result.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            Debug.Log("Download StarterFile");
            foreach (var row in starter)
            {
                var lesson = "";
                lesson += row + ",N|";
                string_array += lesson;
            }
            return string_array;
        }


        public void Upload(string Localpath, string targetname, bool IsString)
        {
            //overload function
            Upload(Localpath, S3UploadBucketName, targetname, IsString);
        }

        public void Upload(string Localpath, string BucketName, string Filename, bool IsString)
        {
            //<summary>
            //Uploads object (Filename) from Localpath directory to S3 bucket (BucketName) 
            //</summary>

            string x = DateTime.Now.ToString(new CultureInfo("en-GB")).Replace(' ', '_');


            var stream = new FileStream(Localpath + Filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            if (IsString)
            {
                var stringstream = GenerateStreamFromString(Filename);

            }


            var request = new PostObjectRequest()
            {
                Bucket = BucketName,
                Key = Filename,
                InputStream = stream,
                CannedACL = S3CannedACL.Private
            };
            AmazonServices.Client.PostObjectAsync(request, (responseObj) =>
            {
                Handle(this, e); //Send upload finished event to event handler
            });
        }

        public void CheckFileExists(string filename, string device_folder,bool IsString) //need to change this name. Nb in other file as well.
        {
            //<summary>
            //Checks filename doesn't already exist in S3. If not, calls Upload 
            //</summary>
            
            e = new FeedbackEventArgs();
            e.type = "upload";
            //credentials ID als Ordner + Timestamp als Dateinamen
            string test = filename;
            var request = new ListObjectsRequest()
            {
                BucketName = S3UploadBucketName
            };
            AmazonServices.Client.ListObjectsAsync(request, (responseObject) =>
            {
                if (responseObject.Exception == null)
                {
                    responseObject.Response.S3Objects.ForEach((o) =>
                    {
                        e.data += o.Key + ",";
                    });
                    if (!e.data.Contains("," + filename + ","))
                    {
                        Upload(device_folder, filename, IsString);
                    }
                    else
                    {
                        Debug.Log("Upload Failure: Filename already exists in S3 bucket");

                    }
                }
            });
        }
        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
